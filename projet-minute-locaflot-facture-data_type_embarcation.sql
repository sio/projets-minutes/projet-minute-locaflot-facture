INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('B1', 'barque', 2, 15.00, 25.00, 80.00, 150.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('B2', 'barque', 4, 30.00, 50.00, 140.00, 200.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('J1', 'jet-ski', 1, 80.00, 150.00, 300.00, 500.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('J2', 'jet-ski', 2, 120.00, 250.00, 400.00, 700.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('M1', 'bateau à moteur', 4, 100.00, 200.00, 400.00, 800.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('P1', 'pédalo simple', 2, 30.00, 55.00, 150.00, 250.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('P2', 'pédalo double', 4, 40.00, 75.00, 170.00, 280.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('P3', 'pédalo toboggan', 6, 45.00, 80.00, 180.00, 300.00);
