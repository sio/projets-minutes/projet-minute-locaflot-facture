GRANT USAGE ON SCHEMA "locaflot_facture" TO "etudiants-slam";
GRANT SELECT ON TABLE "locaflot_facture"."contrat_location" TO "etudiants-slam";
GRANT SELECT ON TABLE "locaflot_facture"."embarcation" TO "etudiants-slam";
GRANT SELECT ON TABLE "locaflot_facture"."louer" TO "etudiants-slam";
GRANT SELECT ON TABLE "locaflot_facture"."type_embarcation" TO "etudiants-slam";
