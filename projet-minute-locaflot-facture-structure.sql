SET client_encoding = 'UTF8';
DROP SCHEMA "locaflot_facture" CASCADE;

CREATE SCHEMA "locaflot_facture";
CREATE TABLE "locaflot_facture"."contrat_location" (
    "id" integer NOT NULL,
    "date" "date",
    "heure_debut" time without time zone,
    "heure_fin" time without time zone
);
CREATE TABLE "locaflot_facture"."embarcation" (
    "id" character varying(4) NOT NULL,
    "couleur" character varying(15),
    "disponible" boolean,
    "type" character varying(2)
);
CREATE TABLE "locaflot_facture"."louer" (
    "id_contrat" integer NOT NULL,
    "id_embarcation" character varying(4) NOT NULL,
    "nb_personnes" integer
);
CREATE TABLE "locaflot_facture"."type_embarcation" (
    "code" character varying(2) NOT NULL,
    "nom" character varying(20),
    "nb_place" integer,
    "prix_demi_heure" numeric(5,2),
    "prix_heure" numeric(5,2),
    "prix_demi_jour" numeric(5,2),
    "prix_jour" numeric(5,2)
);
