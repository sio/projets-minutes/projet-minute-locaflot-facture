ALTER TABLE ONLY "locaflot_facture"."contrat_location"
    ADD CONSTRAINT "pk_contrat_location" PRIMARY KEY ("id");
ALTER TABLE ONLY "locaflot_facture"."embarcation"
    ADD CONSTRAINT "pk_embarcation" PRIMARY KEY ("id");
ALTER TABLE ONLY "locaflot_facture"."louer"
    ADD CONSTRAINT "pk_louer" PRIMARY KEY ("id_contrat", "id_embarcation");
ALTER TABLE ONLY "locaflot_facture"."louer"
    ADD CONSTRAINT "louer_contrat_fkey" FOREIGN KEY ("id_contrat") REFERENCES "locaflot_facture"."contrat_location"("id");
ALTER TABLE ONLY "locaflot_facture"."louer"
    ADD CONSTRAINT "louer_embarcation_fkey" FOREIGN KEY ("id_embarcation") REFERENCES "locaflot_facture"."embarcation"("id");
ALTER TABLE ONLY "locaflot_facture"."type_embarcation"
    ADD CONSTRAINT "pk_type_embarcation" PRIMARY KEY ("code");
ALTER TABLE ONLY "locaflot_facture"."embarcation"
    ADD CONSTRAINT "fk_embarcation" FOREIGN KEY ("type") REFERENCES "locaflot_facture"."type_embarcation"("code");
